import exceptions
import helpers
from language import Type
from language.lexer import Lexem

RootElem = helpers.enum(OR='or', AND='and', NOT='not', PLUS='plus', MINUS='minus', MULT='mult', DIV='div', IDIV='idiv', UMINUS='uminus',
                        LESS='less', LESS_EQUAL='less_equal', MORE='more', MORE_EQUAL='more_equal', EQUAL='equal', NOT_EQUAL='not_equal',
                        CONST='const', VAR='var')

class Expression:  
  def __init__(self, expr_type, root, expr=None, expr_left=None, expr_right=None):
    self.type = expr_type
    self.root = root
    self.expr = expr
    self.expr_left = expr_left
    self.expr_right = expr_right

  def __repr__(self):
    if self.expr != None:
      return "%s(%s)" % (self.root, self.expr)
    else:
      return "%s(%s, %s)" % (self.root, self.expr_left, self.expr_right)

  initialized = False

  @staticmethod
  def init_methods():
    if Expression.initialized:
      return
    Expression.initialized = True

    methods = { RootElem.OR:     {'type': Type.BOOL, 'params': 2},
                RootElem.AND:    {'type': Type.BOOL, 'params': 2},
                RootElem.NOT:    {'type': Type.BOOL, 'params': 1},
                RootElem.PLUS:   {'type': Type.INT,  'params': 2},
                RootElem.MINUS:  {'type': Type.INT,  'params': 2},
                RootElem.MULT:   {'type': Type.INT,  'params': 2},
                RootElem.DIV:    {'type': Type.INT,  'params': 2},
                RootElem.IDIV:   {'type': Type.INT,  'params': 2},
                RootElem.UMINUS: {'type': Type.INT,  'params': 1},
              }

    for root, settings in methods.items():
      if settings['params'] == 1:
        def method_wrapper(root, settings):
          def method(expr):
            return Expression(settings['type'], root, expr=expr)
          return method
      elif settings['params'] == 2:
        def method_wrapper(root, settings):
          def method(expr_left, expr_right):
            return Expression(settings['type'], root, expr_left=expr_left, expr_right=expr_right)
          return method
      else:
        raise exception.InnerException('Invalid settings for expression root = %s, parameters count = %d' % (root, settings['params']))

      setattr(Expression, 'from_' + root, method_wrapper(root, settings))

  @staticmethod
  def from_const(const_type, const):
    return Expression(const_type, RootElem.CONST, expr=const)

  @staticmethod
  def from_var(var_name):
    return Expression(Type.INT, RootElem.VAR, expr=var_name)

  def div(self, x, y):
    if y == 0:
      raise exceptions.DivisionByZeroException()
    return x // y

  def calculate(self, executor):
    calculators = { RootElem.OR: lambda x, y: x or y,
                    RootElem.AND: lambda x, y: x and y,
                    RootElem.NOT: lambda x: not x,
                    RootElem.PLUS: lambda x, y: x + y,
                    RootElem.MINUS: lambda x, y: x - y,
                    RootElem.MULT: lambda x, y: x * y,
                    RootElem.DIV: self.div,
                    RootElem.IDIV: self.div,
                    RootElem.UMINUS: lambda x: -x,
                    RootElem.LESS: lambda x, y: x < y,
                    RootElem.LESS_EQUAL: lambda x, y: x <= y,
                    RootElem.MORE: lambda x, y: x >= y,
                    RootElem.MORE_EQUAL: lambda x, y: x >= y,
                    RootElem.EQUAL: lambda x, y: x == y,
                    RootElem.NOT_EQUAL: lambda x, y: x != y,
                  }
    if self.root in [RootElem.CONST, RootElem.VAR]:
      return executor.get_value(self.expr)
    if self.root not in calculators:
      raise exceptions.InnerException("Can't find calculator for %s" % self.root)
    calculator = calculators[self.root]
    if self.expr != None:
      result = calculator(self.expr.calculate(executor))
    else:
      result = calculator(self.expr_left.calculate(executor), self.expr_right.calculate(executor))
    return result

  def is_const(self):
    return self.root == RootElem.CONST

  def is_var(self):
    return self.root == RootElem.VAR

Expression.init_methods()