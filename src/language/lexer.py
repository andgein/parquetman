import collections
import helpers
from parquet import Cell
import string


Lexem = helpers.enum('KEYWORD', 'VARIABLE', 'BRACKET_OPEN', 'BRACKET_CLOSE', 'SEMICOLON', 'COLON', 'COMMA', 'ASSIGN', 'BLOCK_START', 'BLOCK_FINISH',
                     'CONST_INT', 'CONST_STRING', 'CONST_COLOR', 'VARIABLE', 
                     'END',
                     'LESS', 'LESS_EQUAL', 'MORE', 'MORE_EQUAL', 'NOT_EQUAL', 'EQUAL',
                     'AND', 'OR', 'NOT',
                     'PLUS', 'MINUS', 'MULT', 'DIV', 'IDIV')

class LexemPair(collections.namedtuple('LexemPair', ['lexem', 'value', 'position'])):
  pass


class Lexer:
  RUSSIAN_LETTERS = 'абвгдеёжзийклмнопрстуфхцчшщыьъэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЪЭЮЯ'
  VARIABLES_LETTERS = RUSSIAN_LETTERS + string.digits + string.ascii_letters
  KEYWORDS = {'Перейти на': 'go_to', 'Шаг вправо': 'go_right', 'Шаг влево': 'go_left', 'Шаг вверх': 'go_up', 'Шаг вниз': 'go_down',
              'Положить': 'put', 'Снять плитку': 'pull', 'Запросить': 'input', 'Сообщить': 'output', 'Пауза': 'pause', 'Стоп': 'stop',
              'Если': 'if', 'то': 'then', 'Иначе': 'else', 'Делать пока': 'while', 'Делать от': 'for', 'до': 'to', 
              'с шагом': 'step', 'Покинь': 'break',
              'Программа': 'program', 'Подпрограмма': 'function', 'Вызвать': 'call', 'арг': 'arg', 'рез': 'res', 'цел': 'var_int', 'цвет': 'var_color',
              'справа стена': 'right_wall', 'слева стена': 'left_wall', 'сверху стена': 'up_wall', 'снизу стена': 'down_wall'}
  COLOR_CONSTS = {'к': Cell.RED, 'з': Cell.GREEN}
  WHITESPACES = [' ', '\n', '\r', '\t']
  LEXEM_CONSTS = { Lexem.BRACKET_OPEN: '(',
                   Lexem.BRACKET_CLOSE: ')',
                   Lexem.SEMICOLON: ';',
                   Lexem.COLON: ':',
                   Lexem.COMMA: ',',
                   Lexem.ASSIGN: ':=',
                   Lexem.BLOCK_START: '{',
                   Lexem.BLOCK_FINISH: '}',
                   Lexem.LESS: '<',
                   Lexem.LESS_EQUAL: '<=',
                   Lexem.MORE: '>',
                   Lexem.MORE_EQUAL: '>=',
                   Lexem.NOT_EQUAL: '!=',
                   Lexem.EQUAL: '=',
                   Lexem.AND: 'И',
                   Lexem.OR: 'ИЛИ',
                   Lexem.NOT: 'НЕ',
                   Lexem.PLUS: '+',
                   Lexem.MINUS: '-',
                   Lexem.MULT: '*',
                   Lexem.DIV: '/',
                   Lexem.IDIV: '\\'
                 }
  LEXEM_CONSTS_REVERSE = helpers.reverse_dict(LEXEM_CONSTS)

  def __init__(self, program_text):
    self.program = program_text
    self.position = 0
    self.line = 0
    self.position_in_line = 0
    self.build_letters_list()
    self.read_next_lexem()    

  def build_letters_list(self):
    self.keywords_letters = set()
    for keyword in self.KEYWORDS:
      self.keywords_letters.update(keyword)

  def read_next_lexem(self):
    # Ignore all whitespaces
    while self.position < len(self.program) and self.program[self.position] in self.WHITESPACES:
      self.position += 1

    if self.position >= len(self.program):
      self.current_lexem = Lexem.END
      self.current_value = ""
      return

    # Read keyword
    if self.program[self.position] in self.keywords_letters:
      lexem = ""
      current_position = self.position
      while current_position < len(self.program) and self.program[current_position] in self.keywords_letters:
        lexem += self.program[current_position]
        if lexem in self.KEYWORDS:
          self.current_lexem = Lexem.KEYWORD
          self.current_value = self.KEYWORDS[lexem]
          self.position = current_position + 1
          return
        current_position += 1
    
    # Read colors constants
    for color_const in self.COLOR_CONSTS:
      if self.program[self.position:self.position + len(color_const)] == color_const and \
          (self.position + len(color_const) >= len(self.program) or self.program[self.position + len(color_const)] not in self.RUSSIAN_LETTERS):
        self.current_lexem = Lexem.CONST_COLOR
        self.current_value = self.COLOR_CONSTS[self.program[self.position]]
        self.position += 1
        return

    # Read integers constants
    if self.program[self.position].isdigit():
      current_position = self.position
      lexem = ""
      while current_position < len(self.program) and self.program[current_position].isdigit():
        lexem += self.program[current_position]
        current_position += 1
      self.current_lexem = Lexem.CONST_INT
      self.current_value = int(lexem)
      self.position = current_position
      return      

    # Read constant-string lexems
    for l in range(3, 0, -1):
      lexem = self.program[self.position:self.position + l]
      if lexem in self.LEXEM_CONSTS_REVERSE:
        self.position += l
        self.current_lexem = self.LEXEM_CONSTS_REVERSE[lexem]
        self.current_value = lexem
        return

    # Read variables
    if self.program[self.position] in self.VARIABLES_LETTERS:
      lexem = ""
      while self.program[self.position] in self.VARIABLES_LETTERS:
        lexem += self.program[self.position]
        self.position += 1
      self.current_lexem = Lexem.VARIABLE
      self.current_value = lexem
      return

    raise Exception("Неизвестный токен в позиции %d: \"%s\"" % (self.position, self.program[self.position:self.position + 10]))

  def get_all_lexems(self):
    self.all_lexems = []
    start_position = 0
    while self.current_lexem != Lexem.END:
      end_position = self.position
      self.all_lexems.append(LexemPair(self.current_lexem, self.current_value, helpers.TextFragment(start_position, end_position)))
      start_position = self.position
      self.read_next_lexem()
    end_position = self.position
    self.all_lexems.append(LexemPair(self.current_lexem, self.current_value, helpers.TextFragment(start_position, end_position)))
    return self.all_lexems

      