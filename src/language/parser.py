import collections
import exceptions
import helpers
import language
import language.expressions as expressions
import language.lexer as lexer
import operator
import program
import program.commands as commands

# aliases
Lexem = lexer.Lexem
Type = language.Type
RootElem = expressions.RootElem
VarDeclaration = collections.namedtuple("VarDeclaration", ["name", "type"])

class Parser:

  COMPARE_LEXEMS = { Lexem.LESS: RootElem.LESS,
                     Lexem.LESS_EQUAL: RootElem.LESS_EQUAL,
                     Lexem.MORE: RootElem.MORE,
                     Lexem.MORE_EQUAL: RootElem.MORE_EQUAL,
                     Lexem.NOT_EQUAL: RootElem.NOT_EQUAL,
                     Lexem.EQUAL: RootElem.EQUAL
                   }

  WALL_CONDITIONS = ['left_wall', 'right_wall', 'up_wall', 'down_wall']
 
  def __init__(self):
    pass

  def parse(self, program_text):
    self.lexems = []
    self.lexer = lexer.Lexer(program_text)
    self.lexems = self.lexer.get_all_lexems()

    self.idx = 0
    result_program = program.Program()

    while self.current_lexem.lexem != Lexem.END:
      current_function = self.parse_function()
      result_program.add_function(current_function)

    return result_program

  @property
  def current_lexem(self):
    if len(self.lexems) == 0:
      return self.lexer.all_lexems[-1]
    return self.lexems[self.idx]

  @property
  def prev_lexem(self):
    if self.idx == 0:
      return None
    return self.lexems[self.idx- 1]
  

  def wait_lexem(self, lexem, error_msg=None):
    assert(lexem != Lexem.END)
    assert(self.idx < len(self.lexems))
    ### TODO: Make beautiful error message
    if self.current_lexem.lexem != lexem:
      if lexem in lexer.Lexer.LEXEM_CONSTS:
        lexem = lexer.Lexer.LEXEM_CONSTS[lexem]
      error_msg = error_msg or "Ожидалось \"%s\"" % lexem
      raise exceptions.LexemNotFoundException(error_msg)
    self.idx += 1

  def wait_keyword(self, keyword):
    ### TODO: Beautify error message
    error_msg = "Ожидалось ключевое слово \"%s\"" % keyword
    self.wait_lexem(Lexem.KEYWORD, error_msg=error_msg)
    if self.lexems[self.idx - 1].value != keyword:
      raise exceptions.LexemNotFoundException(error_msg)

  """type var1, var2; type var3, var4;"""
  def parse_var_list(self, at_least_once):
    TYPES = {'var_int': Type.INT, 'var_color': Type.COLOR}

    result = []
    result_var_names = set()
    once = False
    while self.current_lexem.lexem == Lexem.KEYWORD and self.current_lexem.value in TYPES:
      once = True
      self.wait_lexem(Lexem.KEYWORD)
      current_type = TYPES[self.lexems[self.idx - 1].value]
      self.wait_lexem(Lexem.VARIABLE)
      result.append(VarDeclaration(self.lexems[self.idx - 1].value, current_type))
      while self.current_lexem.lexem in [Lexem.COMMA, Lexem.SEMICOLON]:
        if self.current_lexem.lexem == Lexem.SEMICOLON:
          self.idx += 1
          break
        self.idx += 1
        self.wait_lexem(Lexem.VARIABLE)
        var_name = self.lexems[self.idx - 1].value
        if var_name in result_var_names:
          raise exceptions.DuplicateNameException('Переменная %s уже определена здесь' % var_name)
        result.append(VarDeclaration(var_name, current_type))
        result_var_names.add(var_name)

    if at_least_once:
      if not once:
        raise exceptions.ExpectedException('Ожидался тип')
      if len(result) == 0:
        raise exceptions.ExpectedException('Ожидался список переменных')

    return result

  def parse_function(self):
    if self.current_lexem.lexem == Lexem.KEYWORD and self.current_lexem.value in ['program', 'function']:
      is_main = self.current_lexem.value == 'program'
      self.idx += 1
      if is_main:
        if self.current_lexem.lexem == Lexem.VARIABLE:
          self.wait_lexem(Lexem.VARIABLE)
        func_name = program.Program.MAIN_FUNCTION_NAME
      else:
        self.wait_lexem(Lexem.VARIABLE)
        func_name = self.lexems[self.idx - 1].value

      if not is_main:
        """(arg: type Arg1, Arg2; type Arg3; res: Res1, Res2)"""
        arg_vars = []
        result_vars = []
        self.wait_lexem(Lexem.BRACKET_OPEN)
        while self.current_lexem.lexem not in [Lexem.BRACKET_CLOSE, Lexem.END]:
          if self.current_lexem.lexem != Lexem.KEYWORD or self.current_lexem.value not in ['arg', 'res']:
            raise exceptions.ExpectedException('Ожидалось ключевое слово "арг" или "рез"')
          v = arg_vars if self.current_lexem.value == 'arg' else result_vars
          self.idx += 1
          self.wait_lexem(Lexem.COLON)
          var_list = self.parse_var_list(at_least_once=True)
          for var in var_list:
            if var.name in map(operator.itemgetter(0), arg_vars + result_vars):
              raise exceptions.DuplicateNameException('Аргумент %s уже встречался в этой подпрограмме' % var.name)
            v.append(var)
        self.wait_lexem(Lexem.BRACKET_CLOSE)
      else:
        arg_vars = []
        result_vars = []

      local_vars = self.parse_var_list(at_least_once=False)
      for var in local_vars:
        if var.name in map(operator.itemgetter(0), arg_vars + result_vars):
          raise exceptions.DuplicateNameException('Переменная %s уже присутствует в этой подпрограмме' % var.name)

      self.wait_lexem(Lexem.BLOCK_START)
      func_body = self.parse_block(finish_command=commands.ExitFromFunction())
      self.wait_lexem(Lexem.BLOCK_FINISH)
      return program.Function(func_name, arg_vars, result_vars, local_vars, func_body)
    else:
      raise exceptions.ExpectedException('Ожилается подпрограмма или программа')

  def parse_block(self, finish_command=None):
    block_commands = []
    prev_command = None
    start_position = self.current_lexem.position.start
    while not self.current_lexem.lexem in [Lexem.BLOCK_FINISH, Lexem.END]:
      command = self.parse_command()
      block_commands.append(command)
      if prev_command != None:
        prev_command.next_command = command
      command.parent_block, command.next_command = self, None
      prev_command = command

    if finish_command != None:
      block_commands.append(finish_command)      
      if prev_command != None:
        prev_command.next_command = finish_command
      finish_command.parent_block, finish_command.next_command = self, None

    end_position = self.current_lexem.position.start
    block = commands.Block(block_commands)
    block.next_command = None
    block.position = helpers.TextFragment(start_position, end_position)
    return block

  def parse_command(self):
    if self.current_lexem.lexem == Lexem.KEYWORD:
      command = self.parse_keyword()
    elif self.current_lexem.lexem == Lexem.VARIABLE:
      command = self.parse_assign()
    else:
      raise exceptions.ExpectedException('Ожидалась команда')
    ### TODO debug output
    print(command)
    if self.prev_lexem != None and self.prev_lexem.lexem != Lexem.BLOCK_FINISH:
      self.wait_lexem(Lexem.SEMICOLON)
    return command

  def parse_assign(self):
    start_position = self.current_lexem.position.start
    var_name = self.current_lexem.value
    self.idx += 1
    self.wait_lexem(Lexem.ASSIGN)
    expr = self.parse_expression()
    end_position = self.current_lexem.position.start

    return commands.Assign(var_name, expr).setPosition(start_position, end_position)

  def parse_keyword(self):
    start_position = self.current_lexem.position.start
    keyword = self.current_lexem.value
    print("KEYWORD %s" % keyword)
    try:
      parse_method = getattr(self, "parse_keyword_%s" % keyword)
    except AttributeError as e:
      raise exceptions.InternalException(e)

    command = parse_method()
    end_position = self.current_lexem.position.start
    return command.setPosition(start_position, end_position)

  def parse_keyword_go_to(self):
    self.idx += 1
    self.wait_lexem(Lexem.BRACKET_OPEN)
    x_expr = self.parse_expression()
    if x_expr.type != Type.INT:
      raise exceptions.ExpectedException("Ожидалось целое число или выражение")
    self.wait_lexem(Lexem.COMMA)
    y_expr = self.parse_expression()
    if y_expr.type != Type.INT:
      raise exceptions.ExpectedException("Ожидалось целое число или выражение")
    self.wait_lexem(Lexem.BRACKET_CLOSE)
          
    return commands.GoTo(x_expr, y_expr)

  def parse_keyword_go_down(self):
    self.idx += 1
    return commands.GoDown()

  def parse_keyword_go_up(self):
    self.idx += 1
    return commands.GoUp()

  def parse_keyword_go_left(self):
    self.idx += 1
    return commands.GoLeft()

  def parse_keyword_go_right(self):
    self.idx += 1
    return commands.GoRight()

  def parse_keyword_put(self):
    self.idx += 1
    self.wait_lexem(Lexem.BRACKET_OPEN)
    ### TODO: Wait color variable
    self.wait_lexem(Lexem.CONST_COLOR)
    color = self.lexems[self.idx - 1].value
    self.wait_lexem(Lexem.BRACKET_CLOSE)

    return commands.Put(color)

  def parse_keyword_pull(self):
    self.idx += 1
    return commands.Pull()

  """
  IF (<EXPR>) {  
  }
  """
  def parse_keyword_if(self):
    self.idx += 1
    self.wait_lexem(Lexem.BRACKET_OPEN)
    condition_expr = self.parse_expression()
    if condition_expr.type != Type.BOOL:
      raise exceptions.ExpectedException("Ожидалось логическое выражение")
    self.wait_lexem(Lexem.BRACKET_CLOSE)
    self.wait_keyword('then')
    self.wait_lexem(Lexem.BLOCK_START)
    block = self.parse_block()
    self.wait_lexem(Lexem.BLOCK_FINISH)

    if self.current_lexem.lexem == Lexem.KEYWORD and self.current_lexem.value == 'else':
      self.idx += 1
      self.wait_lexem(Lexem.BLOCK_START)      
      else_block = self.parse_block()
      self.wait_lexem(Lexem.BLOCK_FINISH)
    else:
      else_block = None

    return commands.If(condition_expr, block, else_block)

  """
  WHILE (<EXPR>) {  
  }
  """
  def parse_keyword_while(self):
    self.idx += 1
    self.wait_lexem(Lexem.BRACKET_OPEN)
    condition_expr = self.parse_expression()
    if condition_expr.type != Type.BOOL:
      raise exceptions.ExpectedException("Ожидалось логическое выражение")
    self.wait_lexem(Lexem.BRACKET_CLOSE)
    self.wait_lexem(Lexem.BLOCK_START)
    block = self.parse_block()
    self.wait_lexem(Lexem.BLOCK_FINISH)

    return commands.While(condition_expr, block)

  """
  FOR <VAR> := <EXPR> TO <EXPR> {  
  }
  """
  def parse_keyword_for(self):
    self.idx += 1
    self.wait_lexem(Lexem.VARIABLE)
    for_variable = self.lexems[self.idx - 1].value
    self.wait_lexem(Lexem.ASSIGN)

    from_expr = self.parse_expression()
    if from_expr.type != Type.INT:
      raise exceptions.ExpectedException('Ожидалось целое число')
    self.wait_keyword('to')
    to_expr = self.parse_expression()
    if to_expr.type != Type.INT:
      raise exceptions.ExpectedException('Ожидалось целое число')

    self.wait_lexem(Lexem.BLOCK_START)
    block = self.parse_block()
    self.wait_lexem(Lexem.BLOCK_FINISH)

    return commands.For(for_variable, from_expr, to_expr, block)

  def parse_keyword_input(self):
    self.idx += 1
    self.wait_lexem(Lexem.VARIABLE)
    var_name = self.lexems[self.idx - 1].value
    return commands.Input(var_name)

  def parse_keyword_output(self):
    self.idx += 1

    if self.current_lexem.lexem == Lexem.VARIABLE:
      var = self.current_lexem.value
      self.idx += 1
      return commands.Output('var', var)

    if self.current_lexem.lexem == Lexem.CONST_STRING:
      string = self.current_lexem.value
      self.idx += 1
      return commands.Output('text', string)

    raise exceptions.ExpectedException('Ожидалась переменная или строка')

  def parse_keyword_call(self):
    self.idx += 1
    self.wait_lexem(Lexem.VARIABLE)
    func_name = self.lexems[self.idx - 1].value
    self.wait_lexem(Lexem.BRACKET_OPEN)

    args = []

    if self.current_lexem.lexem != Lexem.BRACKET_CLOSE:
      args.append(self.parse_expression())
      while self.current_lexem.lexem in [Lexem.COMMA, Lexem.BRACKET_CLOSE]:
        if self.current_lexem.lexem == Lexem.BRACKET_CLOSE:
          break
        self.idx += 1
        args.append(self.parse_expression())
    self.wait_lexem(Lexem.BRACKET_CLOSE)

    return commands.Call(func_name, args)

  """
  Expression parsing

  BEXPR - boolean expression
  AEXPR - arithmetic expression

  BEXPR = BTERM ([or|xor] BTERM)*
  BTERM = BMULT (and BMULT)*
  BMULT = not BEXPR | '(' BEXPR ')' | VAR | AEXPR [<|>|=|!=|<=|>=] AEXPR

  AEXPR = ATERM ([+|-] ATERM)*
  ATERM = AMULT ([*|/|\] AMULT)*
  AMULT = '-' AEXPR | '(' AEXPR '')' | VAR

  Summary:
  EXPR = TERM t1 (/t1.type == bool/ [or|xor] TERM t2 /t2.type == bool/)* (/t1.type == int/ [+|-] TERM t2 /t2.type == int/)* /result.type = t1.type/
  TERM = ...
  MULT = 
  """
  def parse_expression(self):
    print("Parse expression:", self.current_lexem)
    term = self.parse_term()
    current_expr = term
    if current_expr.type == Type.BOOL:
      while self.current_lexem.lexem == Lexem.OR:
        self.idx += 1
        term = self.parse_term()
        if term.type != Type.BOOL:
          raise exceptions.UnsupportedOperation("ИЛИ применимо только к логическому операнду")
        current_expr = expressions.Expression.from_or(current_expr, term)
    elif current_expr.type == Type.INT:
      while self.current_lexem.lexem in [Lexem.PLUS, Lexem.MINUS]:
        sign_lexem = self.current_lexem.lexem
        self.idx += 1
        term = self.parse_term()
        if term.type != Type.INT:
          raise exceptions.UnsupportedOperation("Сложение и вычитание применимо только к целочисленным операндам")
        ### TODO: create method Expression.from_lexem(sign_lexem, ...)
        if sign_lexem == Lexem.PLUS:
          current_expr = expressions.Expression.from_plus(current_expr, term)
        else:
          current_expr = expressions.Expression.from_minus(current_expr, term)
    else:
      raise exceptions.InternalException("Неверный тип выражения: %d" % current_expr.type)

    sign_lexem = self.current_lexem
    if current_expr.type == Type.INT and sign_lexem.lexem in self.COMPARE_LEXEMS:
      self.idx += 1
      expr_right = self.parse_expression()
      if expr_right.type != Type.INT:
        raise exceptions.UnsupportedOperation("Сравнение применимо только в целочисленным операндам")
      current_expr = expressions.Expression(Type.BOOL, self.COMPARE_LEXEMS[sign_lexem.lexem], expr_left=current_expr, expr_right=expr_right)

    return current_expr

  def parse_term(self):
    print("Parse term:", self.current_lexem)    
    mult = self.parse_mult()
    current_expr = mult
    if current_expr.type == Type.BOOL:
      while self.current_lexem.lexem == Lexem.AND:
        self.idx += 1
        mult = self.parse_mult()
        if mult.type != Type.BOOL:
          raise exceptions.UnsupportedOperation("И применимо только к логическому операнду")
        current_expr = expressions.Expression.from_and(current_expr, mult)
    elif current_expr.type == Type.INT:
      while self.current_lexem.lexem in [Lexem.MULT, Lexem.DIV, Lexem.IDIV]:
        sign_lexem = self.current_lexem.lexem        
        self.idx += 1
        mult = self.parse_mult()
        if mult.type != Type.INT:
          raise exceptions.UnsupportedOperation("Умножение и деление применимо только к целочисленному операнду")
        if sign_lexem == Lexem.MULT:
          current_expr = expressions.Expression.from_mult(current_expr, mult)
        elif sign_lexem == Lexem.DIV:
          current_expr = expressions.Expression.from_div(current_expr, mult)
        else:
          current_expr = expressions.Expression.from_idiv(current_expr, mult)
    else:
      raise exceptions.InternalException("Неверный тип выражения: %s" % current_expr.type)
    return current_expr

  def parse_mult(self):
    print("Parse mult:", self.current_lexem)    
    if self.current_lexem.lexem == Lexem.NOT:
      self.idx += 1
      expr = self.parse_mult()
      if expr.type != Type.BOOL:
        raise exceptions.UnsupportedOperation("НЕ применимо только к логическому операнду")
      return expressions.Expression.from_not(expr)

    if self.current_lexem.lexem == Lexem.MINUS:
      self.idx += 1
      expr = self.parse_mult()
      if expr.type != Type.INT:
        raise exceptions.UnsupportedOperation("Унарный минус применим только к целочисленным операндам")
      return expressions.Expression.from_uminus(expr)

    if self.current_lexem.lexem == Lexem.BRACKET_OPEN:
      self.idx += 1
      expr = self.parse_expression()
      self.wait_lexem(Lexem.BRACKET_CLOSE)
      return expr

    if self.current_lexem.lexem == Lexem.CONST_INT or self.current_lexem.lexem == Lexem.CONST_COLOR:
      if self.current_lexem.lexem == Lexem.CONST_INT:
        const_type = Type.INT
      elif self.current_lexem.lexem == Lexem.CONST_COLOR:
        const_type = Type.BOOL;
      const_value = self.current_lexem.value
      self.idx += 1
      return expressions.Expression.from_const(const_type, const_value)

    if self.current_lexem.lexem == Lexem.KEYWORD and self.current_lexem.value in self.WALL_CONDITIONS:
      const_value = self.current_lexem.value
      self.idx += 1
      return expressions.Expression.from_const(Type.BOOL, const_value)

    if self.current_lexem.lexem == Lexem.VARIABLE:
      var_name = self.current_lexem.value
      self.idx += 1
      return expressions.Expression.from_var(var_name)

    raise exceptions.ExpectedException("Ожидалось выражение")
