import collections
import logging
import sys

class TextFragment(collections.namedtuple('TextFragment', ['start', 'end'])):
  pass


def enum(*sequential, **named):
  enums = dict(zip(sequential, range(len(sequential))), **named)
  return type('Enum', (), enums)

def reverse_dict(d):
  return {v: k for k, v in d.items()}

"""
LazyInit replaces static initializations for classes

@lazyinit("main")
class LazyClass:
  "This method will be run only after initialization"
  def main(self):
    print("main")

  "This method will be run only one time, before all main() calls"
  def __init__(self):
    print("__init__")

>>> LazyClass()
__init__
main
>>> LazyClass()
main
>>> LazyClass()
main
"""
def lazyinit(method):
    def inner(cls):
        def call(*args, **kwds):
          if not hasattr(cls, "instance"):
            cls.instance = cls()
          return getattr(cls.instance, method)(*args, **kwds)
        return call
    return inner

"""
Redirect stdout and stderr to `filename`
"""
def setup_loggers(filename):
  class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
   
    def write(self, buf):
      for line in buf.rstrip().splitlines():
        self.logger.log(self.log_level, line.rstrip())

    def flush(self):
      pass
   
  logging.basicConfig(
     level=logging.DEBUG,
     format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
     filename=filename,
     filemode='a',
     encoding = 'UTF-8'
  )
   
  stdout_logger = logging.getLogger('stdout')
  sl = StreamToLogger(stdout_logger, logging.INFO)
  sys.stdout = sl
   
  stderr_logger = logging.getLogger('stderr')
  sl = StreamToLogger(stderr_logger, logging.ERROR)
  sys.stderr = sl