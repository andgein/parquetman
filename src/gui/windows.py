import codecs
import exceptions
from gui.highlighter import Highlighter
import language.parser as parser
import program.executor as executor
from PySide import QtGui, QtCore
import parquet
import pickle
import time
import threading

class ParquetWindowRepaintSignaller(QtCore.QObject):
  signal = QtCore.Signal(int, int, int, int)
parquet_window_repaint_singaller = ParquetWindowRepaintSignaller()

class ProgramHighlightCommandSingaller(QtCore.QObject):
  signal = QtCore.Signal(int, int)
program_highlight_command_signaller = ProgramHighlightCommandSingaller()

class ExecutionFinishedSignaller(QtCore.QObject):
  signal = QtCore.Signal()
execution_finished_signaller = ExecutionFinishedSignaller()

class ReturnSignaller(QtCore.QObject):
  signal = QtCore.Signal()
return_signaller = ReturnSignaller()

class FroozeSignaller(QtCore.QObject):
  signal = QtCore.Signal(bool)
frooze_signaller = FroozeSignaller()

class ShowMessageSignaller(QtCore.QObject):
  signal = QtCore.Signal(str)
show_message_signaller = ShowMessageSignaller()

class MainWindow(QtGui.QMainWindow):
  def __init__(self, parent=None):
    super(MainWindow, self).__init__(parent)

    self.setWindowTitle('Паркетчик')
    self.setUnifiedTitleAndToolBarOnMac(True)    
    self.resize(640, 512)
    self.showMaximized()
    self.setupMenuBar()
    self.setupStatusBar()

    self.mdiArea = QtGui.QMdiArea()
    self.mdiArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
    self.mdiArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
    self.mdiArea.subWindowActivated.connect(self.none)

    self.parquet_window = ParquetWindow(self)
    self.createMdiChild(self.parquet_window, title='Паркет')
    self.program_window = ProgramWindow(self)
    self.createMdiChild(self.program_window, size=(400, 600), title='Программа')

    self.setCentralWidget(self.mdiArea)

    self.is_debug = False
    self.is_run = False    
    self.program_name = None
    self.parquet_name = None

    show_message_signaller.signal.connect(self.show_error)

  def none(self):
    pass

  def frooze(self):
    frooze_signaller.signal.emit(True)
    #self.program_window.frooze()

  def unfrooze(self):
    program_highlight_command_signaller.signal.emit(0, 0)
    frooze_signaller.signal.emit(False)
    #self.program_window.unfrooze()

  def start_debug(self):
    self.start_parquet = self.parquet_window.parquet.copy()
    
    program_text = self.program_window.toPlainText()
    p = parser.Parser()
    try:
      program = p.parse(program_text)
    except Exception as e:
      show_message_signaller.signal.emit(str(e))
      program_highlight_command_signaller.signal.emit(p.current_lexem.position.start, p.current_lexem.position.end)
      return False
    self.frooze()      
    self.e = executor.Executor(program, self.parquet_window.parquet)
    self.e.error_handler = GuiErrorHandlerWrapper()
    self.step_id = 0
    self.is_debug = True

    self.debug_paused = False
    return True

  def _stop_debug(self):
    self.parquet_window.parquet = self.start_parquet.copy()
    parquet_window_repaint_singaller.signal.emit(0, 0, -1, -1)

    self.unfrooze()
    self.is_debug = False

  def stop_debug(self):
    if not self.is_debug:
      return

    global return_signaller
    return_signaller = ReturnSignaller()
    return_signaller.signal.connect(self._stop_debug)
    execution_finished_signaller.signal.emit()

  def _debug_step(self, repaint_signal, highlight_command_signal):
    if not self.is_debug:
      if not self.start_debug():
        return
    if self.e.finished:
      return

    self.e.step()
    self.parquet_window.parquet = self.e.state.parquet

    if repaint_signal != None:
      repaint_signal.emit(0, 0, -1, -1)
    if highlight_command_signal != None: 
      if self.e.state.current_command != None:
        highlight_command_signal.emit(self.e.state.current_command.position.start, self.e.state.current_command.position.end)
      else:
        highlight_command_signal.emit(0, 0)
    self.step_id += 1
    if self.e.finished:
      self.stop_debug()

  def _run(self, repaint_signal, highlight_command_signal, sleep_time):
    try:
      if not self.is_debug:
        if not self.start_debug():
          return
      self.is_run = True

      while not self.e.finished and not self.debug_paused:
        #print(3)
        call_repaint = True if sleep_time > 0 else self.step_id % 4 == 0
        _repaint_signal = repaint_signal if call_repaint else None

        #print(4)
        self._debug_step(_repaint_signal, highlight_command_signal)
        #print(5)

        if sleep_time > 0:         
          time.sleep(sleep_time)

      self.debug_paused = False
    except Exception as e:
      print("Exception: ", e)
      self.is_run = False
      raise
    self.is_run = False

  def pause(self):
    if self.is_debug:
      self.debug_paused = True
      while self.debug_paused:
        time.sleep(0.1)

  def stop(self):    
    self.pause()
    self.stop_debug()

  def run(self, sleep_time):
    ### Clear highlighting
    program_highlight_command_signaller.signal.emit(0, 0)
    if self.is_run:
      self.pause()

    highlight_command_signal = program_highlight_command_signaller.signal if sleep_time > 0 else None
    global parquet_window_repaint_singaller
    parquet_window_repaint_singaller = ParquetWindowRepaintSignaller()
    parquet_window_repaint_singaller.signal.connect(self.parquet_window.repaint)
    thread = threading.Thread(target=self._run, name="Execute", args=(parquet_window_repaint_singaller.signal, highlight_command_signal, sleep_time))
    thread.start()

  def debug_step(self):
    if self.is_run:
      self.pause()

    global parquet_window_repaint_singaller
    parquet_window_repaint_singaller = ParquetWindowRepaintSignaller()
    parquet_window_repaint_singaller.signal.connect(self.parquet_window.repaint)
    self._debug_step(parquet_window_repaint_singaller.signal, program_highlight_command_signaller.signal)

  def run_slow(self):
    print('Run slow!!!')
    self.run(0.3)

  def run_fast(self):
    print('Run fast!!!')
    self.run(0)

  def add_template(self, template):    
    self.program_window.add_template(template)

  def exit(self):
    self.stop()
    QtGui.qApp.quit()

  def show_error(self, text):
    msgBox = QtGui.QMessageBox()
    msgBox.setWindowTitle('Паркетчик')
    msgBox.setText(text)
    msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
    msgBox.exec_()

  def create_program(self):
    if self.is_debug:
      self.stop()
    self.program_name = None
    self.program_window.setPlainText('')

  def open_program(self):
    filename = QtGui.QFileDialog.getOpenFileName(self, "Открыть программу", filter="Файлы программы (*.pro);;Все файлы (*.*)")
    if filename[0] == '':
      return
    if self.is_debug:
      self.stop()
    self.program_name = filename[0]
    try:
      f = codecs.open(self.program_name, 'r', 'utf-8')
      self.program_window.setPlainText(f.read())
      f.close()
    except Exception as e:
      self.show_error('Не смог прочитать файл: ' + str(e))

  def save_program(self):
    if self.program_name == None:
      self.saveas_program()
    else:
      print("Save program to", self.program_name)
      try:
        f = codecs.open(self.program_name, 'w', 'utf-8')
        f.write(self.program_window.toPlainText())
        f.close()
      except Exception as e:
        self.show_error('Не смог записать файл: ' + str(e))

  def saveas_program(self):
    filename = QtGui.QFileDialog.getSaveFileName(self, "Сохранить как", filter="Файлы программы (*.pro);;Все файлы (*.*)")
    if filename[0] != '':
      self.program_name = filename[0]
      self.save_program()

  def create_parquet(self):
    if self.is_debug:
      self.stop()
    self.parquet_name = None
    self.parquet_window.parquet = parquet.Parquet((40, 30))
    self.parquet_window.repaint()

  def open_parquet(self):
    filename = QtGui.QFileDialog.getOpenFileName(self, "Открыть паркет", filter="Файлы паркета (*.prk);;Все файлы (*.*)")
    if filename[0] == '':
      return
    self.parquet_name = filename[0]
    if self.is_debug:
      self.stop()
    try:
      f = open(self.parquet_name, 'rb')
      self.parquet_window.parquet = pickle.load(f)
      f.close()
      self.parquet_window.repaint()
    except Exception as e:
      self.show_error('Не смог прочитать файл: ' + str(e))

  def save_parquet(self):
    if self.parquet_name == None:
      self.saveas_parquet()
    else:
      print("Save parquet to", self.parquet_name)
      try:
        f = open(self.parquet_name, 'wb')
        pickle.dump(self.parquet_window.parquet, f)
        f.close()
      except Exception as e:
        self.show_error('Не смог записать файл: ' + str(e))

  def saveas_parquet(self):
    filename = QtGui.QFileDialog.getSaveFileName(self, "Сохранить паркет как", filter="Файлы паркета (*.prk);;Все файлы (*.*)")
    if filename[0] != '':    
      self.parquet_name = filename[0]
      self.save_parquet()    

  def setupMenuBar(self):
    program_menu = QtGui.QMenu('&Программа', self)
    program_menu.addAction('&Создать программу', self.create_program, 'Ctrl+N')    
    program_menu.addAction('&Открыть программу', self.open_program, 'Ctrl+O')
    program_menu.addAction('Со&хранить программу', self.save_program, 'Ctrl+S')
    program_menu.addAction('Сохранить программу &как', self.save_program, 'Ctrl+Shift+S')
    program_menu.addAction('&Выход', self.exit, 'Ctrl+Q')

    parquet_menu = QtGui.QMenu('П&аркет', self)
    parquet_menu.addAction('&Создать паркет', self.create_parquet, 'Ctrl+M')
    parquet_menu.addAction('&Открыть паркет', self.open_parquet, 'Ctrl+P')
    parquet_menu.addAction('Со&хранить паркет', self.save_parquet, 'Ctrl+T')
    parquet_menu.addAction('Сохранить паркет &как', self.save_parquet, 'Ctrl+Shift+T')

    run_menu = QtGui.QMenu('&Выполнение', self)
    run_menu.addAction('Запустить &медленно', self.run_slow, 'F5')
    run_menu.addAction('Запустить &быстро', self.run_fast, 'F7')
    run_menu.addAction('Сделать один &шаг', self.debug_step, 'F8')
    run_menu.addAction('&Приостановить', self.pause, 'F2')
    run_menu.addAction('&Закончить выполнение', self.stop, 'F10')

    template_menu = QtGui.QMenu('&Шаблоны', self)

    action_menu = QtGui.QMenu('Действия', template_menu)
    action_menu.addAction('Шаг вправо;', lambda: self.add_template('Шаг вправо;'))
    action_menu.addAction('Шаг влево;', lambda: self.add_template('Шаг влево;'))
    action_menu.addAction('Шаг вверх;', lambda: self.add_template('Шаг вверх;'))
    action_menu.addAction('Шаг вниз;', lambda: self.add_template('Шаг вниз;'))
    action_menu.addAction('Положить( );', lambda: self.add_template('Положить(!);'))
    action_menu.addAction('Снять плитку;', lambda: self.add_template('Снять плитку;'))
    action_menu.addAction(':= ;', lambda: self.add_template(':= ;'))
    action_menu.addAction('Запросить ;', lambda: self.add_template('Запросить !;'))
    action_menu.addAction('Сообщить ;', lambda: self.add_template('Сообщить !;'))
    action_menu.addAction('Пауза( );', lambda: self.add_template('Пауза(!);'))
    action_menu.addAction('Перейти на ( , );', lambda: self.add_template('Перейти на (!, );'))
    template_menu.addMenu(action_menu)

    condition_menu = QtGui.QMenu('Условия', template_menu)
    condition_menu.addAction('слева стена', lambda: self.add_template('слева стена'))
    condition_menu.addAction('справа стена', lambda: self.add_template('справа стена'))
    condition_menu.addAction('сверху стена', lambda: self.add_template('сверху стена'))
    condition_menu.addAction('снизу стена', lambda: self.add_template('снизу стена'))
    condition_menu.addAction('к', lambda: self.add_template('к'))
    condition_menu.addAction('з', lambda: self.add_template('з'))
    condition_menu.addAction('НЕ', lambda: self.add_template('НЕ'))
    condition_menu.addAction('ИЛИ', lambda: self.add_template('ИЛИ'))
    condition_menu.addAction('И', lambda: self.add_template('И'))
    template_menu.addMenu(condition_menu)

    if_menu = QtGui.QMenu('Ветвления', template_menu)
    if_menu.addAction('Если () то {}', lambda: self.add_template('Если (!) то {\n\n}'))
    if_menu.addAction('Иначе {}', lambda: self.add_template('Иначе {\n  !\n}'))
    template_menu.addMenu(if_menu)    

    loop_menu = QtGui.QMenu('Циклы', template_menu)
    loop_menu.addAction('Делать пока () {}', lambda: self.add_template('Делать пока (!) {\n\n}'))
    loop_menu.addAction('Делать от := до {}', lambda: self.add_template('Делать от ! :=  до  {\n\n}'))
    template_menu.addMenu(loop_menu)

    subprogram_menu = QtGui.QMenu('Подпрограммы', template_menu)
    subprogram_menu.addAction('Программа {}', lambda: self.add_template('Программа {\n  !\n}'))
    subprogram_menu.addAction('Подпрограмма {}', lambda: self.add_template('Подпрограмма ! {\n\n}'))
    subprogram_menu.addAction('Вызвать ;', lambda: self.add_template('Вызвать !;'))
    template_menu.addMenu(subprogram_menu)

    declaration_menu = QtGui.QMenu('Описания', template_menu)
    declaration_menu.addAction('цел ;', lambda: self.add_template('цел !;'))
    declaration_menu.addAction('цвет ;', lambda: self.add_template('цвет !;'))

    
    self.menuBar().addMenu(program_menu)
    self.menuBar().addMenu(parquet_menu)
    self.menuBar().addMenu(run_menu)
    self.menuBar().addMenu(template_menu)

  def setupStatusBar(self):
    self.statusBar().showMessage('Status bar')

  def createMdiChild(self, child, size=None, title=None):
    sub_window = self.mdiArea.addSubWindow(child)
    if size != None:
      sub_window.setFixedSize(size[0], size[1])
    if title != None:
      sub_window.setWindowTitle(title)

    #child.copyAvailable.connect(self.cutAct.setEnabled)
    #child.copyAvailable.connect(self.copyAct.setEnabled)

    return sub_window

  def activeMdiChild(self):
    activeSubWindow = self.mdiArea.activeSubWindow()
    if activeSubWindow:
      return activeSubWindow.widget()
    return None



class ParquetWindow(QtGui.QWidget):
  def __init__(self, parent=None):
    super(ParquetWindow, self).__init__(parent)
    self.CELL_PICTURE_SIZE = 20
    self.width = 40
    self.height = 30
    self.setFixedSize(self.width * self.CELL_PICTURE_SIZE, (self.height + 1) * self.CELL_PICTURE_SIZE + 2)
    self.parquet = parquet.Parquet((self.width, self.height))
    self.pixmaps = { parquet.Cell.EMPTY : QtGui.QPixmap('resources/empty.png'),
                     parquet.Cell.WALL : QtGui.QPixmap('resources/wall.png'),
                     parquet.Cell.RED : QtGui.QPixmap('resources/red.png'),
                     parquet.Cell.GREEN : QtGui.QPixmap('resources/green.png')
                   }
    self.cursor_pixmal = QtGui.QPixmap('resources/cursor.png')
    self.current_mouse = None
    self.mouse_positions = [parquet.Cell.EMPTY, parquet.Cell.GREEN, parquet.Cell.RED, parquet.Cell.WALL, 'M']
    self.mouse_texts = ['Очистить клетку', 'Положить зелёную плитку', 'Положить красную плитку', 'Поставить стенку', 'Переместить паркетчика']

  def paintEvent(self, event):
    painter = QtGui.QPainter(self)
    for i in range(self.width):
      for j in range(self.height):
        painter.drawPixmap(self.CELL_PICTURE_SIZE * i,
                           self.CELL_PICTURE_SIZE * (self.height - j - 1),
                           self.pixmaps[self.parquet[i][j]])
    painter.drawPixmap(self.CELL_PICTURE_SIZE * self.parquet.man.x,
                       self.CELL_PICTURE_SIZE * (self.height - self.parquet.man.y - 1),
                       self.cursor_pixmal)
    j = -1
    for i in range(5):
      painter.drawPixmap(self.CELL_PICTURE_SIZE * i,
                         self.CELL_PICTURE_SIZE * (self.height - j - 1) + 2,
                         self.pixmaps[self.mouse_positions[i]] if i < 4 else self.cursor_pixmal)
    
    painter.drawLine(0, self.height * self.CELL_PICTURE_SIZE,
                     self.CELL_PICTURE_SIZE * self.width, self.height * self.CELL_PICTURE_SIZE)
    painter.drawLine(0, self.height * self.CELL_PICTURE_SIZE + 1,
                     self.CELL_PICTURE_SIZE * self.width, self.height * self.CELL_PICTURE_SIZE + 1)

    if self.current_mouse != None:
      text_rect = QtCore.QRect(self.CELL_PICTURE_SIZE * 6, self.CELL_PICTURE_SIZE * (self.height - j - 1) + 2, 500, self.CELL_PICTURE_SIZE)
      text = self.mouse_texts[self.mouse_positions.index(self.current_mouse)]
      font = QtGui.QFont()
      font.setFamily('Courier')
      font.setFixedPitch(True)
      font.setPointSize(12)
      painter.setFont(font)
      painter.drawText(text_rect, QtCore.Qt.AlignLeft or QtCore.Qt.AlignVCenter, text)

  def mousePressEvent(self, event):
    x = event.x()
    y = event.y()
    if y >= self.height * self.CELL_PICTURE_SIZE:      
      _type = x // self.CELL_PICTURE_SIZE
      if _type < 5:
        self.current_mouse = self.mouse_positions[_type]
        self.repaint()
    else:
      y = (y + self.CELL_PICTURE_SIZE - 1) // self.CELL_PICTURE_SIZE
      x //= self.CELL_PICTURE_SIZE
      y = self.height - y
      if self.current_mouse in self.mouse_positions[:3]:
        self.parquet[x][y] = self.current_mouse
      if self.current_mouse == parquet.Cell.WALL:
        if self.parquet.man.x != x or self.parquet.man.y != y:
          self.parquet[x][y] = self.current_mouse
      if self.current_mouse == 'M':
        if self.parquet[x][y] != parquet.Cell.WALL:
          self.parquet.man = parquet.Coords(x, y)
      if self.current_mouse != None:
        self.repaint()



class ProgramWindow(QtGui.QTextEdit):
  def __init__(self, parent=None):
    super(ProgramWindow, self).__init__(parent)

    self.highlighter = Highlighter(self.document())
    self.resize(500, 500)

    font = QtGui.QFont()
    font.setFamily('Courier')
    font.setFixedPitch(True)
    font.setPointSize(12)
    self.setFont(font)

    self.setPlainText('''Программа 
{
  Делать пока (НЕ сверху стена)
  {
    Шаг вверх;
  }
}''')

    program_highlight_command_signaller.signal.connect(self.highlight_command)
    execution_finished_signaller.signal.connect(self.execution_finished)
    frooze_signaller.signal.connect(self.frooze_wrapper)

  def execution_finished(self):
    msgBox = QtGui.QMessageBox()
    msgBox.setWindowTitle('Паркетчик')
    msgBox.setText('Выполнение завершено')
    msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
    msgBox.setDefaultButton(QtGui.QMessageBox.Ok)
    ret = msgBox.exec_()

    return_signaller.signal.emit()

  def highlight_command(self, start, end):
    ### Erase format
    fmt = QtGui.QTextCharFormat()
    fmt.setBackground(QtCore.Qt.white)

    cursor = QtGui.QTextCursor(self.document())
    cursor.setPosition(0, QtGui.QTextCursor.MoveAnchor)
    cursor.setPosition(len(self.toPlainText()) - 1, QtGui.QTextCursor.KeepAnchor)
    cursor.setCharFormat(fmt)

    ### Set new format
    fmt = QtGui.QTextCharFormat()
    fmt.setBackground(QtCore.Qt.lightGray)

    cursor = QtGui.QTextCursor(self.document())
    cursor.setPosition(start, QtGui.QTextCursor.MoveAnchor)
    cursor.setPosition(end, QtGui.QTextCursor.KeepAnchor)
    cursor.setCharFormat(fmt)

  def frooze_wrapper(self, is_frooze):
    if is_frooze:
      self.frooze()
    else:
      self.unfrooze()

  def frooze(self):
    self.setReadOnly(True)

  def unfrooze(self):
    self.setReadOnly(False)

  def add_template(self, template):
    cursor = self.textCursor()
    position = cursor.position()
    text = self.toPlainText()
    new_text = text[:position] + template.replace('!', '') + text[position:]
    self.setPlainText(new_text)

    new_cursor = QtGui.QTextCursor(self.document())
    if template.find('!') == -1:      
      new_cursor.setPosition(position + len(template))      
    else:
      new_cursor.setPosition(position + template.find('!'))
    self.setTextCursor(new_cursor)    
      
  def keyPressEvent(self, event):
    ### Auto indent
    if event.key() == QtCore.Qt.Key_Return:
      program = self.toPlainText()

      cursor = self.textCursor()
      position = cursor.position() - 1
      while position >= 0:
        if program[position] == '\n':
          break
        position -= 1

      append = ''
      while program[position + 1] in [' ', '\t']:
        append += program[position + 1]
        position += 1

      self.add_template('\n' + append)
    else:
      super(ProgramWindow, self).keyPressEvent(event)

class GuiErrorHandlerSignaller(QtCore.QObject):
  signal = QtCore.Signal(Exception)
gui_error_handler_signaller = GuiErrorHandlerSignaller()

class GuiErrorHandlerWrapper:
  def raise_error(self, exception):
    error_handler.processed = False
    gui_error_handler_signaller.signal.emit(exception)    
    while not error_handler.processed:
      time.sleep(0.1)

class GuiErrorHandler:
  def __init__(self):
    global gui_error_handler_signaller
    gui_error_handler_signaller = GuiErrorHandlerSignaller()
    gui_error_handler_signaller.signal.connect(self.raise_error)

  def show_error(self, text):
    msgBox = QtGui.QMessageBox()
    msgBox.setWindowTitle('Паркетчик')
    msgBox.setText(text)
    msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
    msgBox.exec_()
    self.processed = True

  def raise_error(self, exception):
    print(exception)
    if type(exception) is exceptions.OutOfParquetException:
      self.show_error('Паркетчик вышел за пределы поля')
    elif type(exception) is exceptions.GoToWallException:
      self.show_error('Паркетчик наткнулся на стену')
    elif type(exception) is exceptions.PullFromEmptyCellException:
      self.show_error('Нельзя снять плитку с пустой ячейки')
    elif type(exception) is exceptions.PutToBusyCellException:
      self.show_error('Нельзя положить плитку на занятую ячейку')
    elif type(exception) is exceptions.DivisionByZeroException:
      self.show_error('Деление на ноль!')
    elif type(exception) is exceptions.RuntimeException:
      self.show_error(str(exception))
    else:
      self.show_error('Неизвестная ошибка: ' + str(exception))
      raise exception

error_handler = GuiErrorHandler()
