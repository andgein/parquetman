from PySide import QtGui, QtCore
import language.lexer as lexer

class Highlighter(QtGui.QSyntaxHighlighter):
  def __init__(self, parent=None):
    super(Highlighter, self).__init__(parent)

    keywordFormat = QtGui.QTextCharFormat()
    keywordFormat.setForeground(QtCore.Qt.darkBlue)
    keywordFormat.setFontWeight(QtGui.QFont.Bold)

    #keywordPatterns = ["\\bchar\\b", "\\bclass\\b", "\\bconst\\b",
    #    "\\bdouble\\b", "\\benum\\b", "\\bexplicit\\b", "\\bfriend\\b",
    #    "\\binline\\b", "\\bint\\b", "\\blong\\b", "\\bnamespace\\b",
    #    "\\boperator\\b", "\\bprivate\\b", "\\bprotected\\b",
    #    "\\bpublic\\b", "\\bshort\\b", "\\bsignals\\b", "\\bsigned\\b",
    #    "\\bslots\\b", "\\bstatic\\b", "\\bstruct\\b",
    #    "\\btemplate\\b", "\\btypedef\\b", "\\btypename\\b",
    #    "\\bunion\\b", "\\bunsigned\\b", "\\bvirtual\\b", "\\bvoid\\b",
    #    "\\bvolatile\\b"]

    keywordPatterns = list(map(self.addWorkBreaks, lexer.Lexer.KEYWORDS.keys()))

    self.highlightingRules = [(QtCore.QRegExp(pattern), keywordFormat)
        for pattern in keywordPatterns]


    constFormat = QtGui.QTextCharFormat()
    constFormat.setForeground(QtCore.Qt.red)
    constFormat.setFontWeight(QtGui.QFont.Bold)
    constPatterns = list(lexer.Lexer.LEXEM_CONSTS.values())
    for idx, constPattern in enumerate(constPatterns):
      if constPattern[0] in lexer.Lexer.RUSSIAN_LETTERS:
        constPatterns[idx] = self.addWorkBreaks(constPattern)
      else:
        constPatterns[idx] = QtCore.QRegExp.escape(constPattern)
    self.highlightingRules.extend([(QtCore.QRegExp(pattern), constFormat)
                                      for pattern in constPatterns])

    self.multiLineCommentFormat = QtGui.QTextCharFormat()
    self.multiLineCommentFormat.setForeground(QtCore.Qt.red)

    quotationFormat = QtGui.QTextCharFormat()
    quotationFormat.setForeground(QtCore.Qt.darkGreen)
    self.highlightingRules.append((QtCore.QRegExp("\".*\""),
        quotationFormat))

    functionFormat = QtGui.QTextCharFormat()
    functionFormat.setFontItalic(True)
    functionFormat.setForeground(QtCore.Qt.blue)
    self.highlightingRules.append((QtCore.QRegExp("\\b[A-Za-z0-9_]+(?=\\()"),
        functionFormat))

    self.commentStartExpression = QtCore.QRegExp("(\\*")
    self.commentEndExpression = QtCore.QRegExp("\\*)")

  def addWorkBreaks(self, text):
    ### TODO Invalid highlighting of brackets in ``Положить(з)``    
    return "\\b" + QtCore.QRegExp.escape(text) + "\\b"

  def highlightBlock(self, text):
    for pattern, format in self.highlightingRules:
      expression = QtCore.QRegExp(pattern)
      index = expression.indexIn(text)
      while index >= 0:
        length = expression.matchedLength()
        self.setFormat(index, length, format)
        index = expression.indexIn(text, index + length)

    self.setCurrentBlockState(0)

    startIndex = 0
    if self.previousBlockState() != 1:
      startIndex = self.commentStartExpression.indexIn(text)

    while startIndex >= 0:
      endIndex = self.commentEndExpression.indexIn(text, startIndex)

      if endIndex == -1:
        self.setCurrentBlockState(1)
        commentLength = len(text) - startIndex
      else:
        commentLength = endIndex - startIndex + self.commentEndExpression.matchedLength()

      self.setFormat(startIndex, commentLength,
          self.multiLineCommentFormat)
      startIndex = self.commentStartExpression.indexIn(text,
          startIndex + commentLength);
