import os
import helpers
import copy

import exceptions
import parquet.coords as coords

Cell = helpers.enum(EMPTY=' ', WALL='*', RED='R', GREEN='G')
Coords = coords.Coords

class Parquet:
  
  def __init__(self, size):
    self.width, self.height = size
    self.field = [[Cell.EMPTY] * self.height for _ in range(self.width)]
    self.man = Coords(0, 0)

  def __repr__(self):
    result = 'Parquet %d x %d. Man at (%d, %d).' % (self.width, self.height, self.man.x, self.man.y) + os.linesep
    for row in self.field:
      result += ''.join(row) + os.linesep
    return result 

  def __getitem__(self, key):
    if isinstance(key, slice):
      return [self[i] for i in range(*key.indices(len(self.field)))]
    elif isinstance(key, int):
      return self.field[key]

  def copy(self):
    return copy.deepcopy(self)

  def man_cell(self):
    return self.field[self.man.x][self.man.y]

  def in_field(self, position):
    return 0 <= position.x < self.width and 0 <= position.y < self.height    

  def go_to(self, position):
    if isinstance(position, tuple) and len(position) == 2:
      position = Coords._make(position)
    if not self.in_field(position):
      raise exceptions.OutOfParquetException()
    if self.field[position.x][position.y] == Cell.WALL:
      raise exceptions.GoToWallException()
    self.man = position

  def go_up(self):
    self.go_to(self.man.up())

  def go_down(self):
    self.go_to(self.man.down())

  def go_left(self):
    self.go_to(self.man.left())

  def go_right(self):
    self.go_to(self.man.right())

  def put(self, what):
    if self.man_cell() != Cell.EMPTY:
      raise exceptions.PutToBusyCellException()
    self.field[self.man.x][self.man.y] = what

  def pull(self):
    if self.man_cell() == Cell.EMPTY:
      raise exceptions.PullFromEmptyCellException()
    self.field[self.man.x][self.man.y] = Cell.EMPTY

  def is_wall(self, position):
    if not self.in_field(position):
      return True
    return self.field[position.x][position.y] == Cell.WALL