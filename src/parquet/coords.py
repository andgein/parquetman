import collections

class Coords(collections.namedtuple('Coords', ['x', 'y'])):
  def up(self):
    return Coords(self.x, self.y + 1)

  def down(self):
    return Coords(self.x, self.y - 1)

  def left(self):
    return Coords(self.x - 1, self.y)

  def right(self):
    return Coords(self.x + 1, self.y)
