class InternalException(Exception):
  
  def __init__(self, exception):
    self.inner_exception = exception

  def __str__(self):
    return "InternalException:\nInner exception:\n%s" % str(self.inner_exception)


class ParserException(Exception):
  pass

class LexemNotFoundException(ParserException):
  pass

class UnsupportedOperation(ParserException):
  pass

class ExpectedException(ParserException):
  pass

class FunctionAlreadyExists(ParserException):
  pass

class DuplicateNameException(ParserException):
  pass


class MainNotFoundException(Exception):
  pass

class RuntimeException(Exception):
  pass

class OutOfParquetException(RuntimeException):
  pass

class GoToWallException(RuntimeException):
  pass  

class PullFromEmptyCellException(RuntimeException):
  pass

class PutToBusyCellException(RuntimeException):
  pass

class DivisionByZeroException(RuntimeException):
  pass

class DivisionByZero(RuntimeException):
  pass

class UnknownVariable(RuntimeException):
  pass

class UseUninitializedVariable(RuntimeException):
  pass

class FunctionNotFound(RuntimeException):
  pass

class InvalidParameters(RuntimeException):
  pass