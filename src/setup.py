import sys

from cx_Freeze import setup, Executable

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
        name = "Parquetman",
        version = "1.0",
        description = "Parquetman is a programing language for children",
        executables = [Executable("main.py", base = base)])