from parquet import Coords

class Action:
  def apply(self, executor):
    pass

  def __str__(self):
    repr_str = "%sAction" % self.__class__.__name__
    if hasattr(self, 'action_params'):
      repr_str += '(%s)' % (', '.join(map(str, self.action_params)))
    return repr_str



class Error(Action):
  
  def __init__(self, exception):
    self.exception = exception

  def apply(self, executor):
    executor.error_handler.raise_error(self.exception)


class GoTo(Action):
  
  def __init__(self, position):
    if isinstance(position, tuple) and len(position) == 2:
      position = Coords._make(position)
    if not isinstance(position, Coords):
      raise ValueError('Invalid position in GoTo(). Must be tuple (x, y) or Coords(x, y)')
    self.position = position
    self.action_params = [position]

  def apply(self, executor):
    executor.state.parquet.go_to(self.position)


class Stop(Action):

  def __init__(self):
    pass

  def apply(self, executor):
    executor.stop()


class SetNextCommand(Action):
  
  def __init__(self, command):
    self.command = command
    self.action_params = [command]

  def apply(self, executor):
    executor.state.current_command = self.command


class GoUp(Action):
  def apply(self, executor):
    executor.state.parquet.go_up()

class GoDown(Action):
  def apply(self, executor):
    executor.state.parquet.go_down()

class GoLeft(Action):
  def apply(self, executor):
    executor.state.parquet.go_left()

class GoRight(Action):
  def apply(self, executor):
    executor.state.parquet.go_right()

class Put(Action):  
  def __init__(self, what):
    self.what = what
    self.action_params = [what]

  def apply(self, executor):
    executor.state.parquet.put(self.what)

class Pull(Action):
  def apply(self, executor):
    executor.state.parquet.pull()

class Assign(Action):
  def __init__(self, var_name, value):
    self.var_name = var_name
    self.value = value
    self.action_params = [var_name, value]

  def apply(self, executor):
    executor.assign(self.var_name, self.value)

class Input(Action):
  def __init__(self, var_name):
    self.var_name = var_name
    self.action_params = [var_name]

  def apply(self, executor):
    executor.assign(self.var_name, executor.io_handler.input())

class Output(Action):
  def __init__(self, output_type, message):
    self.type = output_type
    self.message = message
    self.action_params = [output_type, message]

  def apply(self, executor):
    if self.type == 'text':
      executor.io_handler.output(self.message)
    else:
      executor.io_handler.output(executor.get_value(self.message))

# class Call(Action):
#   def __init__(self, func_name, args):
#     self.func_name = func_name
#     self.args = args
#     self.action_params = [func_name, args]

#   def apply(self, executor):
#     pass