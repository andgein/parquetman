import copy
import exceptions
import helpers
import program.actions as actions
from parquet import Coords

class Command:
  def __init__(self, *args, **kwargs):
    self.next_command = None
    self.command_params = args
    self.position = helpers.TextFragment(0, 0)
  
  def execute(self, executor):
    raise exceptions.InternalException("Somebody forgot define %s.execute() method" % (self.__class__.__name__))

  def __repr__(self):
    repr_str = "%sCommand" % self.__class__.__name__
    if hasattr(self, 'command_params'):
      repr_str += '(%s)' % (', '.join(map(str, self.command_params)))
    repr_str += ':(%d,%d)' % (self.position.start, self.position.end) 
    return repr_str

  def setPosition(self, position):
    self.position = position
    return self

  def setPosition(self, start, end):
    self.position = helpers.TextFragment(start, end)
    return self


class GoTo(Command):
  def __init__(self, x_expr, y_expr):
    Command.__init__(self, x_expr, y_expr)
    self.x_expr = x_expr
    self.y_expr = y_expr

  def execute(self, executor):
    try:
      position = (self.x_expr.calculate(executor), self.y_expr.calculate(executor))
    except Exception as e:
      return [actions.Error(e)]

    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.go_to(position)
    except Exception as e:
      return [actions.Error(e)]
    return [actions.GoTo(position)]


class Stop(Command):
  def execute(self, executor):
    return [actions.Stop()]


class GoUp(Command):
  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.go_up()
    except Exception as e:
      return [actions.Error(e)]
    return [actions.GoUp()]

class GoDown(Command):
  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.go_down()
    except Exception as e:
      return [actions.Error(e)]
    return [actions.GoDown()]

class GoLeft(Command):
  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.go_left()
    except Exception as e:
      return [actions.Error(e)]
    return [actions.GoLeft()]

class GoRight(Command):
  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.go_right()
    except Exception as e:
      return [actions.Error(e)]
    return [actions.GoRight()]

    
class Put(Command):
  def __init__(self, what):
    Command.__init__(self, what)    
    self.what = what

  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.put(self.what)
    except Exception as e:
      return [actions.Error(e)]
    return [actions.Put(self.what)]
    
class Pull(Command):
  def execute(self, executor):
    parquet_copy = executor.state.parquet.copy()
    try:
      parquet_copy.pull()
    except Exception as e:
      return [actions.Error(e)]
    return [actions.Pull()]


class If(Command):
  def __init__(self, condition, block, else_block=None):
    Command.__init__(self, condition, block, else_block)    
    self.condition = condition
    self.block = block
    self.else_block = else_block
    if else_block != None:
      self.command_params = [condition, block, else_block]
    else:
      self.command_params = [condition, block]

  def execute(self, executor):
    self.block.next_command = self.next_command
    if self.else_block != None:
      self.else_block.next_command = self.next_command    
    try:
      cond_value = self.condition.calculate(executor)
    except Exception as e:
      return [actions.Error(e)]

    if cond_value:
      return [actions.SetNextCommand(self.block)]
    elif self.else_block != None:
      return [actions.SetNextCommand(self.else_block)]
    else:
      return []

class While(Command):
  def __init__(self, condition, block):
    Command.__init__(self, condition, block)
    self.condition = condition
    self.block = block

  def execute(self, executor):
    self.block.next_command = self
    try:
      cond_value = self.condition.calculate(executor)
    except Exception as e:
      return [actions.Error(e)]

    if cond_value:
      return [actions.SetNextCommand(self.block)]
    else:
      return []

class For(Command):
  def __init__(self, var_name, from_expr, to_expr, block):
    Command.__init__(self, var_name, from_expr, block)    
    self.var_name = var_name
    self.from_expr = from_expr
    self.to_expr = to_expr
    self.block = block

  def execute(self, executor):
    from_value = self.from_expr.calculate(executor)
    to_value = self.to_expr.calculate(executor)

    helper = ForHelper(self.var_name, to_value, self.block)
    helper.next_command = self.next_command
    helper.position = self.position
    self.block.next_command = helper
    return [actions.Assign(self.var_name, from_value - 1), actions.SetNextCommand(helper)]

class ForHelper(Command):
  def __init__(self, var_name, to_value, block):
    Command.__init__(self, var_name, to_value, block)
    self.var_name = var_name
    self.to_value = to_value
    self.block = block

  def execute(self, executor):
    current_value = executor.get_value(self.var_name) + 1
    if current_value <= self.to_value:
      return [actions.Assign(self.var_name, current_value), actions.SetNextCommand(self.block)]
    else:
      return [actions.Assign(self.var_name, current_value)]

class Block(Command):
  def __init__(self, commands):
    Command.__init__(self, commands)
    self.commands = commands

  def execute(self, executor):
    if len(self.commands) == 0:
      return []
    self.commands[-1].next_command = self.next_command
    return [actions.SetNextCommand(self.commands[0])]

class Assign(Command):
  def __init__(self, var_name, expr):
    Command.__init__(self, var_name, expr)    
    self.var_name = var_name
    self.expr = expr

  def execute(self, executor):
    value = self.expr.calculate(executor)
    return [actions.Assign(self.var_name, value)]

class Input(Command):
  def __init__(self, var_name):
    Command.__init__(self, var_name)    
    self.var_name = var_name

  def execute(self, executor):
    return [actions.Input(self.var_name)]

class Output(Command):
  def __init__(self, output_type, message):
    Command.__init__(self, output_type, message)    
    self.type = output_type
    self.message = message

  def execute(self, executor):
    return [actions.Output(self.type, self.message)]

class Call(Command):
  def __init__(self, func_name, args):
    Command.__init__(self, func_name, args)    
    self.func_name = func_name
    self.args = args

  def execute(self, executor):
    func = executor.program.get_function(self.func_name)
    if len(func.args) != len(self.args):
      raise exceptions.InvalidParameters('Found %d parameters, must be %d' % (len(self.args), len(func.args)))
    for i in range(len(func.arg_vars), len(func.args)):
      if not self.args[i].is_var():
        raise exceptions.InvalidParameters('Parameter %d must be l-value' % (i + 1))
    for i in range(len(func.args)):
      if self.args[i].type != func.args[i].type:
        raise exceptions.InvalidParameters('Parameter %d must be %s, but is %s' % (i + 1, func.args[i].type, self.args[i].type))

    args_values = []
    for arg in self.args[:len(func.arg_vars)]:
      args_values.append(arg.calculate(executor))

    executor.stack.push_state()
    executor.stack.push(func.result_vars)
    executor.stack.push(self.args[len(func.arg_vars):])
    executor.stack.push(self.next_command)
    executor.state.variables.clear()
    for i in range(len(func.arg_vars)):
      executor.state.variables.add_variable(func.arg_vars[i].name, func.arg_vars[i].type, args_values[i])
    for var in func.result_vars:
      executor.state.variables.add_variable(var.name, var.type)
    for var in func.local_vars:
      executor.state.variables.add_variable(var.name, var.type)

    return [actions.SetNextCommand(func.get_first_command())]

class ExitFromFunction(Command):
  def execute(self, executor):
    command = executor.stack.pop()
    outer_vars = executor.stack.pop()
    inner_vars = executor.stack.pop()
    inner_variables = copy.deepcopy(executor.state.variables)
    executor.stack.pop_state()
    for outer, inner in zip(outer_vars, inner_vars):
      executor.state.variables.set_value(outer.expr, inner_variables.get_value(inner.name))
    return [actions.SetNextCommand(command)]
