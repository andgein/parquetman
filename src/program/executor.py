import collections
import copy
import exceptions
from parquet import Parquet, Coords
import program.actions
import program.commands as commands
from language.lexer import Lexer

class Executor:
  
  def __init__(self, program, parquet):
    self.program = program
    self.state = ExecutionState(program, parquet)
    self.stack = ExecutionStack(self)
    self.error_handler = ErrorHandler()
    self.io_handler = ConsoleIOHandler()
    self.stopped = False

  def stop(self):
    self.stopped = True

  @property
  def finished(self):
    return self.state.current_command == None

  def step(self):
    if self.stopped:
      return False
    
    current_command = self.state.current_command
    print("Command:", current_command)
    actions = current_command.execute(self)
    print("Actions:", ', '.join(map(str, actions)))
    next_command_is_set = False
    for action in actions:
      action.apply(self)
      # TODO check error_handler, may be action called it?
      if type(action) is program.actions.SetNextCommand:
        next_command_is_set = True

    if not next_command_is_set:
      self.state.current_command = current_command.next_command

  def get_value(self, var_name):
    return self.state.get_value(var_name)

  def assign(self, var_name, value):
    self.state.assign(var_name, value)

class ErrorHandler:
  
  def __init__(self):
    pass

  def raise_error(self, exception):
    if type(exception) is exceptions.OutOfParquetException:
      print("RE: Out of parquet...")
    elif type(exception) is exceptions.GoToWallException:
      print("RE: Go to wall...")
    elif type(exception) is exceptions.RuntimeException:
      print("RE: Runtime exception: %s" % str(exception))
    else:
      print("RE: %s" % str(exception))


class ConsoleIOHandler:
  def output(self, message):
    print(message)

  def input(self):
    return int(input())


class ExecutionState:
  WALL_CONDITIONS = { 'right_wall': Coords.right,
                      'left_wall':  Coords.left,
                      'up_wall':    Coords.up,
                      'down_wall':  Coords.down
                    }

  def __init__(self, program, parquet):
    self.parquet = parquet
    self.variables = VarStorage()
    self.program = program
    self.current_command = commands.Call(program.MAIN_FUNCTION_NAME, [])

  def get_value(self, var_name):
    if type(var_name) == int:
      return var_name
    elif var_name in Lexer.COLOR_CONSTS.values():
      return self.parquet.man_cell() == var_name
    elif var_name in self.WALL_CONDITIONS:
      return self.parquet.is_wall(self.WALL_CONDITIONS[var_name](self.parquet.man))
    else:
      return self.variables.get_value(var_name)

  def assign(self, var_name, value):
    self.variables.set_value(var_name, value)


class ExecutionStack:
  def __init__(self, executor):
    self.executor = executor
    self.stack = []

  def push_state(self):
    self.push(copy.deepcopy(self.executor.state.variables))
    self.push(copy.deepcopy(self.executor.state.current_command))
#    self.push(copy.deepcopy(self.executor.state))

  def push(self, obj):
    self.stack.append(obj)

  def pop_state(self):
#    tmp_parquet = copy.deepcopy(self.executor.state.parquet)
#    self.executor.state = self.pop()
#    self.executor.state.parquet = tmp_parquet
    self.executor.state.current_command = self.pop()
    self.executor.state.variables = self.pop()


  def pop(self):
    if len(self.stack) == 0:
      raise exceptions.InternalException('ExecutionStack.pop() doesn\'t work on empty stack')
    result = self.stack[-1]
    self.stack.pop()
    return result

class Variable:
  def __init__(self, var_name, var_type, is_init, value):
    self.name = var_name
    self.type = var_type
    self.is_init = is_init
    self.value = value    
   
  def __repr__(self):
    if not self.is_init:
      return '(Undef %s:%s)' % (self.name, self.type)
    else:
      return '(%s:%s = %s)' % (self.name, self.type, str(self.value))

class VarStorage():
  def __init__(self):
    self.clear()

  def __repr__(self):
    return str(list(self.variables.values()))

  def add_variable(self, var_name, var_type, value=None):
    self.variables[var_name] = Variable(var_name, var_type, value != None, value)

  def get_value(self, var_name):
    if var_name not in self.variables:
      raise exceptions.UnknownVariable('Unknown variable: %s' % var_name)
    if not self.variables[var_name].is_init:
      raise exceptions.UseUninitializedVariable('Use uninitialized variable: %s' % var_name)
    return self.variables[var_name].value

  def set_value(self, var_name, value):
    if var_name not in self.variables:
      raise exceptions.UnknownVariable('Unknown variable: %s' % var_name)
    self.variables[var_name].is_init = True
    self.variables[var_name].value = value

  def clear(self):
    self.variables = {}