import program.commands as commands
import exceptions

class Program:

  MAIN_FUNCTION_NAME = "__main__"

  def __init__(self):
    self.functions = {}

  def get_function(self, func_name):
    if func_name not in self.functions:
      raise exceptions.FunctionNotFound()
    return self.functions[func_name]
    
  def add_function(self, function):
    if function.name in self.functions:
      raise exceptions.FunctionAlreadyExists()
    self.functions[function.name] = function
    return self

  def get_first_command(self):
    if not self.MAIN_FUNCTION_NAME in self.functions:
      raise exceptions.MainNotFoundException()
    
    return self.functions[self.MAIN_FUNCTION_NAME].body

  def __repr__(self):
    repr_str = "Program\n  Functions:\n"
    for function_name, function in self.functions.items():
      repr_str += "    %s: %s" % (function_name, str(function)) + "\n"
    return repr_str
      

class Function:
  def __init__(self, name, arg_vars, result_vars, local_vars, body):
    self.name = name
    self.arg_vars = arg_vars
    self.result_vars = result_vars
    self.local_vars = local_vars
    self.body = body
    self.args = self.arg_vars + self.result_vars

  def __repr__(self):
    return '(%s; %s): %s' % (', '.join(map(str, self.arg_vars)), ', '.join(map(str, self.result_vars)), str(self.body))

  def get_first_command(self):
    return self.body
