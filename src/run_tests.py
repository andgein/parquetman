import glob
import unittest

# Change to 1 for cutting output
VERBOSITY = 2

testmodules = glob.glob("tests/*.py")
# Replace \ to . and remove .py in filenames
testmodules = map(lambda s: s[0:-3].replace("\\", "."), testmodules)

suite = unittest.TestSuite()
for module in testmodules:
  print(module, unittest.defaultTestLoader.loadTestsFromName(module))
  suite.addTest(unittest.defaultTestLoader.loadTestsFromName(module))

# Run tests
unittest.TextTestRunner(verbosity=VERBOSITY).run(suite)