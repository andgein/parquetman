import gui.application
import gui.windows
import sys
import subprocess
import helpers

if __name__ == "__main__":
  if len(sys.argv) > 1:
    helpers.setup_loggers('parquetman.log')
    app = gui.application.Application()
    window = gui.windows.MainWindow()
    window.show()
    sys.exit(app.exec_())
  else:
    if sys.platform == "win32":
      DETACHED_PROCESS = 8
      subprocess.Popen(sys.argv[0] + " run", creationflags=DETACHED_PROCESS, close_fds=True)
    else:
      subprocess.Popen(sys.argv[0] + " run", close_fds=True)