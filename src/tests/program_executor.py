import unittest
import program
import program.commands as commands
import exceptions
import program.executor as executor

class Test(unittest.TestCase):

  def setUp(self):
    self.program = program.Program()

  def test_execute_go_up(self):
    command1 = commands.GoTo((4, 4))
    command2 = commands.GoUp()
    command3 = commands.Stop()
    command1.next_command = command2
    command2.next_command = command3

    function = program.Function(self.program.MAIN_FUNCTION_NAME).add_command(command1)
    self.program.add_function(function)    

    self.executor = executor.Executor(self.program, (15, 15))
    self.executor.step()

    self.assertEquals(self.executor.state.parquet.man, (4, 4))

    self.executor.step()

    self.assertEquals(self.executor.state.parquet.man, (4, 5))

