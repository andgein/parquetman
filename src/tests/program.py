import unittest
import program
import exceptions
import program.executor as executor

class Test(unittest.TestCase):

  def setUp(self):
    self.program = program.Program()

  def test_add_function_to_program(self):
    function = program.Function("function_name")
    self.program.add_function(function)

    self.assertEqual(len(self.program.functions), 1)
    self.assertEqual(self.program.functions["function_name"].name, "function_name")

  def test_execute_program_without_main_function(self):
    function = program.Function("function_name")
    self.program.add_function(function)

    self.assertRaises(exceptions.MainNotFoundException, self.program.get_first_command)
    
  @unittest.skip
  def test_execute_program(self):
    function = program.Function(self.program.MAIN_FUNCTION_NAME)
    self.program.add_function(function)

    self.executor = executor.Executor(self.program, (15, 15))
    self.executor.step()

      